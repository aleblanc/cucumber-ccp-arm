cmake_minimum_required(VERSION 2.8.12)

project(Cucumber-Cpp)


set(CUKE_USE_STATIC_BOOST ON CACHE BOOL "Statically link Boost (except boost::test)")
set(CUKE_USE_STATIC_GTEST ON CACHE BOOL "Statically link Google Test")

set(CUKE_DISABLE_BOOST_TEST OFF CACHE BOOL "Disable boost:test")
set(CUKE_DISABLE_GTEST OFF CACHE BOOL "Disable Google Test framework")
set(CUKE_DISABLE_UNIT_TESTS OFF CACHE BOOL "Disable unit tests")
set(GOOGLETEST_DIR  googletest )
set(GMOCK_VER "1.7.0" CACHE STRING "Google Mock framework version to be used")

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)


#
# Boost
#

set(BOOST_MIN_VERSION "1.40")

set(CUKE_CORE_BOOST_LIBS thread system regex date_time program_options)
if(NOT CUKE_DISABLE_BOOST_TEST)
    set(CUKE_TEST_BOOST_LIBS unit_test_framework)
endif()

if(CUKE_USE_STATIC_BOOST)
    set(CUKE_STATIC_BOOST_LIBS ${CUKE_CORE_BOOST_LIBS})
    # "An external test runner utility is required to link with dynamic library" (Boost User's Guide)
    set(CUKE_DYNAMIC_BOOST_LIBS ${CUKE_TEST_BOOST_LIBS})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_TEST_DYN_LINK")

    find_package(Threads)
    set(CUKE_EXTRA_LIBRARIES ${CUKE_EXTRA_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
else()
    set(CUKE_DYNAMIC_BOOST_LIBS ${CUKE_CORE_BOOST_LIBS} ${CUKE_TEST_BOOST_LIBS})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_ALL_DYN_LINK")
endif()

set(Boost_USE_STATIC_RUNTIME OFF)
if(CUKE_STATIC_BOOST_LIBS)
    set(Boost_USE_STATIC_LIBS ON)
    find_package(Boost ${BOOST_MIN_VERSION} COMPONENTS ${CUKE_STATIC_BOOST_LIBS})
endif()
if(CUKE_DYNAMIC_BOOST_LIBS)
    set(Boost_USE_STATIC_LIBS OFF)
    find_package(Boost ${BOOST_MIN_VERSION} COMPONENTS ${CUKE_DYNAMIC_BOOST_LIBS})
endif()

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
    set(CUKE_EXTRA_LIBRARIES 
        ${CUKE_EXTRA_LIBRARIES} 
        ${Boost_THREAD_LIBRARY} 
        ${Boost_SYSTEM_LIBRARY} 
        ${Boost_REGEX_LIBRARY} 
        ${Boost_DATE_TIME_LIBRARY} 
        ${Boost_PROGRAM_OPTIONS_LIBRARY})
endif()



#
# GTest
#

if(NOT CUKE_DISABLE_GTEST)
    set(GTEST_DIR ${GOOGLETEST_DIR}/googletest)
    set(GTEST_INCLUDE_DIR  ${GTEST}/include)
    set(GTEST_INCLUDE_DIRS ${GTEST_INCLUDE_DIR})
    set(GTEST_FOUND TRUE)

    include_directories(
            ${GTEST_DIR}/include
            ${GTEST_DIR})

    add_library(libgtest STATIC ${GTEST_DIR}/src/gtest-all.cc)
    add_library(libgtest_main STATIC ${GTEST_DIR}/src/gtest_main.cc)
    target_link_libraries(libgtest_main libgtest)

    set(GTEST_BOTH_LIBRARIES libgtest libgtest_main)

    set(CUKE_GTEST_LIBRARIES
    ${GTEST_BOTH_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT}
    )
    set (GMOCK_DIR ${GOOGLETEST_DIR}/googlemock)
        
    include_directories(${GMOCK_DIR}/include
            ${GMOCK_DIR}
            ${GTEST_DIR}/include
            ${GTEST_DIR})
        
    add_library(libgmock STATIC
            ${GTEST_DIR}/src/gtest-all.cc
            ${GMOCK_DIR}/src/gmock-all.cc)

    add_library(libgmock_main STATIC
            ${GTEST_DIR}/src/gtest-all.cc
            ${GMOCK_DIR}/src/gmock-all.cc
            ${GMOCK_DIR}/src/gmock_main.cc)
        
    set(GMOCK_BOTH_LIBRARIES libgmock libgmock_main)

    set(CUKE_GMOCK_LIBRARIES
    ${GTEST_BOTH_LIBRARIES}
    ${GMOCK_BOTH_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT}
    )
endif()


set(CUKE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

include_directories(${CUKE_INCLUDE_DIR})

set(CUKE_LIBRARIES cucumber-cpp ${CUKE_EXTRA_LIBRARIES})

add_subdirectory(src)

if(NOT DTEST_SOURCE AND NOT DTEST_BUILD_DIR)
    set(DTEST_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/../sfl_cucumber_tests/features/linux_cpp)
    set(DTEST_BUILD_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../sfl_cucumber_tests/features/linux_cpp/build)
endif()

add_subdirectory(${DTEST_SOURCE} ${DTEST_BUILD_DIR})

