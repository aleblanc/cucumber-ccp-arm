#cross compile options

set(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_CROSSCOMPILING TRUE)
INCLUDE(CMakeForceCompiler)

SET(CMAKE_FIND_ROOT_PATH /usr/arm-linux-gnueabihf)
# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_CXX_COMPILER /usr/bin/arm-linux-gnueabihf-g++-6)
set(CMAKE_C_COMPILER /usr/bin/arm-linux-gnueabihf-gcc-6)

set(BOOST_INCLUDEDIR /usr/arm-linux-gnueabihf/include)
set(BOOST_LIBRARYDIR /usr/arm-linux-gnueabihf/lib)